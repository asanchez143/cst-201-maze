// std dependencies
#include <algorithm>
#include <string>
#include <vector>
#include <iostream>
#include <tuple>

// Self-dependencies
#include "vectors.cpp"

std::tuple<int, int, int, int, int, int> getHeaderValues(const std::string& input) {
	const std::string size = splitstring(input, ".", 0);			// Getting the first delimiter for size

	const int sizex = std::stoi(splitstring(size, ",", 0));			// Getting the first value from the , delimiter then parsing it as an int
	const int sizey = std::stoi(splitstring(size, ",", 1));			// Getting the second value from the , delimiter then parsing it as an int

	const std::string startlocat = splitstring(input, ".", 1);		// Getting the second delimiter for location of the start flag

	const int startx = std::stoi(splitstring(startlocat, ",", 0));	// Getting the first value from the , delimiter then parsing it as an int
	const int starty = std::stoi(splitstring(startlocat, ",", 1));	// Getting the second value from the , delimiter then parsing it as an int

	const std::string endlocat = splitstring(input, ".", 2);		// Getting the third delimiter for location of the end flag

	const int endx = std::stoi(splitstring(endlocat, ",", 0));		// Getting the first value from the , delimiter then parsing it as an int
	const int endy = std::stoi(splitstring(endlocat, ",", 1));		// Getting the second value from the , delimiter then parsing it as an int

	return {sizex, sizey, startx, starty, endx, endy};
}

/// <summary>
/// 	Loads the visualized grid.
/// </summary>
///
/// <param name='input'>
/// 	The input string loaded from the input file.
/// </param>
///
/// <returns>
/// 	A std::string with all of the grid spots.
/// </returns>
std::string load(const std::string& input) {
	
	auto [sizex, sizey, startx, starty, endx, endy] = getHeaderValues(input);

	/*
	 *
	 *	The header is now finished. Moving onto the main grid.
	 *
	 */

	// Create char array equal to the size we should output
	char outputarr [sizey * 3 + 1][sizex * 3 + 1];
	memset(outputarr, ' ', (sizey * 3 + 1) * (sizex * 3 + 1));
/*
	// This was done to clean up the data, since it's not cleaned to 0x00
	for (int i = 0; i < sizey * 3 + 1; i++) {
		outputarr.insert(LinkedList<char>());

		for (int j = 0; j < sizex * 3 + 1; j++) {
			outputarr.get(i)->value.insert(' ');
			//outputarr[i][j] = ' ';
		}
	}*/

	// Iterating through the x values to add the grid #'s
	for (int i = 1; i < sizex * 3 + 1; i += 3) {
		std::string unloadedint = std::to_string(i / 3);	// Retrieve the true value of i
		const char *cstr = unloadedint.c_str();				// Turning string into char array

		bool fullsize = false;

		// In case the size is three, we need to step back for it to display properly
		if (sizeof(cstr) == 3) {
			fullsize = true;
		}

		int ival = (fullsize ? i : i - 1) - 1;

		// Copying the true value of i into its spot
		for (int j = ival; j < unloadedint.length() + ival; j++) {
			outputarr[0][j] = cstr[j - ival];
		}
	}

	// Iterating through the y values to add the grid #'s
	for (int i = 1; i < sizey * 3 + 1; i += 3) {
		std::string unloadedint = std::to_string(i / 3);	// Retrieve the true value of i
		const char *cstr = unloadedint.c_str();				// Turning string into char array

		bool fullsize = false;

		// In case the size is three, we need to step back for it to display properly
		if (sizeof(cstr) == 3) {
			fullsize = true;
		}

		int ival = (fullsize ? i : i - 1) - 1;

		// Copying the true value of i into its spot
		for (int j = ival; j < unloadedint.length() + ival; j++) {
			outputarr[j][0] = cstr[j - ival];
		}
	}

	std::string stringvector = splitstring(input, ".", 3);
	std::vector<Point*> pointvector = strtopoint(stringvector);

	// Checking each grid spot. If it's a start or end flag, then generate the cell. If not, then check if it's a wall or not.
	for (int x = 1; x < sizex + 1; x++) {
		for (int y = 1; y < sizey + 1; y++) {
			if ((x - 1 == startx && y - 1 == starty) || (x - 1 == endx && y - 1 == endy)) {		// Has to be - 1 due to the extra spot in the array for the grid numbers

				// Subdividing the grid into a 3x3 grid to draw it correctly
				for (int subx = x * 3 - 2; subx < x * 3 + 1; subx++) {
					for (int suby = y * 3 - 2; suby < y * 3 + 1; suby++) {
						outputarr[suby][subx] = '.';
					}
				}

				if (x - 1 == startx && y - 1 == starty)
					outputarr[y * 3 - 1][x * 3 - 1] = 'S';

				else
					outputarr[y * 3 - 1][x * 3 - 1] = 'F';
			}

			else {

				// Creating a testpoint that has the grid spot's x/y value to see if it's in the point vector
				Point *testpoint = new Point(x - 1, y - 1);

				for (int i = 0; i < pointvector.size(); i++) {
					if (pointvector.at(i)->x == testpoint->x && pointvector.at(i)->y == testpoint->y) {
						for (int subx = x * 3 - 2; subx < x * 3 + 1; subx++) {
							for (int suby = y * 3 - 2; suby < y * 3 + 1; suby++) {
								outputarr[suby][subx] = '#';
							}
						}

						// Make sure to break since it'll check every other vector otherwise
						break;
					}

					else {
						for (int subx = x * 3 - 2; subx < x * 3 + 1; subx++) {
							for (int suby = y * 3 - 2; suby < y * 3 + 1; suby++) {
								outputarr[suby][subx] = '.';
							}
						}
					}
				}
			}
		}
	}

	std::string output = "";

	// Since C++ doesn't clean the data before allocating the array, it can cause unintended binary data to enter the array. This converts them all to spaces
	// I also had to swap the x and the y so it'd display properly (since for arrays, the first value is actually its y value instead of its x)
	for (int x = 0; x < sizey * 3 + 1; x++) {
		for (int y = 0; y < sizex * 3 + 1; y++) {
			if (!isdigit(outputarr[x][y]) && outputarr[x][y] != 'S' && outputarr[x][y] != 'F' && outputarr[x][y] != '.' && outputarr[x][y] != '#') {
				output += ' ';
			}

			else {
				output += outputarr[x][y];
			}
		}

		output += '\n';
	}

	return output;
}