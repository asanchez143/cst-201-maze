// std dependencies
#include <string>
#include <vector>

// Self-dependencies
#include "grid.h"

/// <summary>
/// 	Cleans the string for later parsing.
/// </summary>
///
/// <param name='input'>
/// 	The input string to clean.
/// </param>
///
/// <returns>
/// 	Returns a cleaned std::string.
/// </returns>
std::string clean(const std::string& input) {
	std::string output = "";

	// Iterate through the string, removing specified chars
	for (std::string::const_iterator i = input.begin(); i != input.end(); i++) {
		if (*i != '\n' && *i != ' ' && *i != 0x00 && *i != '\t' && *i != '(' && *i != ')') {
			output += *i;
		}
	}

	return output;
}

/// <summary>
/// 	Splits a std::string based on a delimiter, returning a vector of std::string pointers. Also cleans input.
/// </summary>
///
/// <param name='input'>
/// 	The input string to be checked for the delimiter.
/// </param>
///
/// <param name='delimiter'>
/// 	The delimiter being check for in the input.
/// </param>
///
/// <param name='start'>
/// 	Where to start the split count at.
/// </param>
///
/// <param name='end'>
/// 	Where to end the split count at. Setting this to 0 will make it ignore limits.
/// </param>
///
/// <returns>
/// 	Returns a vector of std::string split with the delimiter based on `start` and `end` boundaries.
/// </returns>
///
/// TODO: Add error when out of bounds on `start` and `end`
std::vector<std::string> splitstring(const std::string& input, const std::string& delimiter, const unsigned int start, const unsigned int end) {
	std::string checkstr = "";
	std::string delimcheck = "";
	std::vector<std::string> output;

	int counter = 0;	// Keeping count for the `start` and `end` parameters

	// Loop through each character of the initial string
	for (std::string::const_iterator i = input.begin(); i != input.end(); i++) {

		// Check for the delimiter, terminating character (0x00), then check to make sure the line isn't empty for a push
		if (delimcheck == delimiter || *i == 0x00) {
			if (!checkstr.empty()) {

				// Check to make sure that the counter isn't out of bounds
				if (counter >= start) {
					std::string cleanedstring = clean(checkstr.substr(0, checkstr.size() - delimiter.size()));	// This one's a bit messy, but I'm removing the
					output.push_back(cleanedstring);															// delimiter from the substring, cleaning it,
																												// then pushing it
					// Check to see if the counter is over the given end position
					if (counter > end && end != 0) {
						return output;
					}
				}
			}

			checkstr.clear();	// Clear the checkstr for the next output
			delimcheck.clear();	// Clear the delimcheck for the next delimiter check

			counter++;			// Iterate counter for `start` and `end` checks

			// If end conditions are met, drop out of the function and return
			if (*i == 0x00 || (counter > end && end != 0)) {
				return output;
			}
		}

		checkstr += *i;		// Accumulate the next character into the checkstr
		delimcheck += *i;	// Accumulate the next character into the delimiter check

		// If delimiter size is over max, drop first char
		if (delimcheck.size() > delimiter.size()) {
			delimcheck = delimcheck.substr(1, delimcheck.size() - 1);
		}
	}

	// Check to make sure it didn't terminate with accumulated values
	if (!checkstr.empty())
		 output.push_back(clean(checkstr));

	return output;
}

/// <summary>
/// 	Splits a std::string based on a delimiter, returning a single std::string. Also cleans input.
/// </summary>
///
/// <param name='input'>
/// 	The input string to be checked for the delimiter.
/// </param>
///
/// <param name='delimiter'>
/// 	The delimiter being check for in the input.
/// </param>
///
/// <param name='pos'>
/// 	Which split to return
/// </param>
///
/// <returns>
/// 	Returns a single std::string split with the delimiter at specified position.
/// </returns>
///
/// TODO: Add error when out of bounds on `pos`
std::string splitstring(const std::string& input, const std::string& delimiter, const unsigned int pos) {
	std::string output = "";
	std::string delimcheck = "";

	std::vector<std::string> vectorsplit = splitstring(input, delimiter, 0, 0);

	return vectorsplit.at(pos);
}

/// <summary>
/// 	Turns a vector of string pointers into a vector of Point pointers.
/// </summary>
///
/// <param name='stringvector'>
/// 	The string pointer vector to be converted to a Point pointer vector.
/// </param>
///
/// <returns>
/// 	Returns a vector of Point pointers. Be sure to properly dispose of the string vector when completed using `delete [var-name].at(i)` in a loop.
/// </returns>
std::vector<Point*> strvectortopoint(const std::vector<std::string>& stringvector) {
	std::vector<Point*> output;

	// Loop through each vector value, then turn into a point
	for (int i = 0; i < stringvector.size(); i++) {
		const std::string tempstr = stringvector.at(i);				// Loading the string for the sake of lessening the verbosity

		const int x = std::stoi(splitstring(tempstr, ",", 0));		// Loading the x value using the previous `splitstring` method
		const int y = std::stoi(splitstring(tempstr, ",", 1));		// Loading the y value using the previous `splitstring` method

		Point *p = new Point(x, y);	// Creating Point object

		output.push_back(p);		// Pushing the Point 
	}

	return output;
}

std::vector<Point*> strtopoint(const std::string& string) {
	std::vector<Point*> output;
	std::vector<std::string> split = splitstring(string, ",", 0, 0);

	for (int i = 0; i < split.size(); i += 2) {
		const std::string tempstr = split.at(i) + ',' + split.at(i + 1);	// Loading the string for the sake of lessening the verbosity

		const int x = std::stoi(splitstring(tempstr, ",", 0));		// Loading the x value using the previous `splitstring` method
		const int y = std::stoi(splitstring(tempstr, ",", 1));		// Loading the y value using the previous `splitstring` method

		Point *p = new Point(x, y);	// Creating Point object

		output.push_back(p);		// Pushing the Point

	}
	return output;
}