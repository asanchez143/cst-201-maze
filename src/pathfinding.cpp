#include <vector>
#include <stdlib.h>
#include <float.h>
#include <tuple>
#include <iostream>
struct Cell {
	Cell *parent;
	Cell *next;
	int x, y;
	double f;
};

bool inBounds(Point p, int sizex, int sizey) {
	return (p.x >= 0) && (p.x < sizex) &&
		   (p.y >= 0) && (p.y < sizey);
}

bool isBlocked(char **grid, bool **impasses, Point p) {
	return grid[p.y * 3 + 1][p.x * 3 + 1] == '#' && !impasses[p.y][p.x];
}

bool isEnd(Point *curr, Point end) {
	return (curr->x == end.x) && (curr->y == end.y);
}

double heuristic(Point p, Point end) {
	return ((double) sqrt((p.y - end.y) * (p.y - end.y) + (p.x - end.x) * (p.x - end.x)));
}

std::tuple<Cell*, bool*> search(char **grid, bool **impasses, Cell *curr, Point end) {
	std::cout << "GOT HERE\n";
	double cardinals [8];

	Cell *smallest;
	double smallestHeur = DBL_MAX;
	std::cout << "GOT HERE2\n";
	std::cout << (curr->x == NULL ? "true\n" : "false\n");
	std::cout << curr->x << ", " << curr->y << '\n';
	for (int i = curr->y - 1; i < curr->y + 1; i++) {
		for (int j = curr->x - 1; j < curr->x + 1; j++) {
			std::cout << ((i == curr->y && j == curr->x) || (i == curr->parent->y && j == curr->parent->x) || impasses[i][j] ? "true\n" : "false\n");
			if ((i == curr->y && j == curr->x) || (i == curr->parent->y && j == curr->parent->x) || impasses[i][j])
				continue;
			std::cout << "GOT HERE3\n";
			Point p = Point{j, i};

			if (!isBlocked(grid, impasses, p)) {
				double f = heuristic(p, end);
				std::cout << "GOT HERE5\n";
				if (f < smallestHeur) {
					smallestHeur = f;
					smallest = new Cell{curr, NULL, j, i, f};
					curr->next = smallest;
					std::cout << "GOT HERE6\n";
				}
			}
		}
	}
	std::cout << "GOT HERE7\n";
	if (smallest == NULL) {
		impasses[curr->y][curr->x] = true;
	}
	std::cout << "GOT HERE8\n";
	return {smallest, *impasses};
}

char** constructNext(char **gridspots, Cell cell) {
	if (cell.next->x == cell.x + 1) {
		if (cell.next->y == cell.y + 1) {
			gridspots[2][2] = '\\';
		}

		else if (cell.next->y == cell.y) {
			gridspots[1][2] = '-';
		}

		else {
			gridspots[0][2] = '/';
		}
	}

	else if (cell.next->x == cell.x) {
		if (cell.next->y == cell.y + 1) {
			gridspots[2][1] = '|';
		}

		else {
			gridspots[0][1] = '|';
		}
	}

	else {
		if (cell.next->y == cell.y + 1) {
			gridspots[0][0] = '\\';
		}

		else if (cell.next->y == cell.y) {
			gridspots[1][0] = '-';
		}

		else {
			gridspots[2][0] = '/';
		}
	}

	return gridspots;
}

char** constructPrevious(Cell cell) {
	char **gridspots;

	memset(gridspots, '.', 9);
	gridspots[1][1] = 'X';

	if (cell.parent->x == cell.x - 1) {
		if (cell.parent->y == cell.y - 1) {
			gridspots[0][0] = '\\';
		}

		else if (cell.parent->y == cell.y) {
			gridspots[1][0] = '-';
		}

		else {
			gridspots[2][0] = '/';
		}
	}

	else if (cell.parent->x == cell.x) {
		if (cell.parent->y == cell.y - 1) {
			gridspots[0][1] = '|';
		}

		else {
			gridspots[2][1] = '|';
		}
	}

	else {
		if (cell.parent->y == cell.y + 1) {
			gridspots[0][2] = '/';
		}

		else if (cell.parent->y == cell.y) {
			gridspots[1][2] = '-';
		}

		else {
			gridspots[2][2] = '\\';
		}
	}

	return gridspots;
}

char** reconstruct(Cell cell) {
	return constructNext(constructPrevious(cell), cell);
}