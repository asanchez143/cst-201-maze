#include <string>
#include <iostream>
#include <fstream>
#include <tuple>

#include "visualize.cpp"
#include "pathfinding.cpp"
#include "stack.h"

using namespace std;

int main(int argc, char *argv[]) {
	string input = "";

	fstream rf;
	rf.open(argv[1], ios::in);

	while (!rf.eof()) {
		char ch;
		rf >> ch;

		input += ch;
	}
	std::cout << "Got here\n";
	std::string output = load(input);
	std::cout << "Got here2\n";
	Stack<Cell> *s = new Stack<Cell>();

	auto [sizex, sizey, startx, starty, endx, endy] = getHeaderValues(input);
	//std::cout << "Got here3\n" << sizex;

	char *grid = (char*)malloc((sizex * 3) * (sizey * 3) * sizeof(char));
	memset(grid, '.', (sizex * 3) * (sizey * 3));

	for (int i = 1; i < sizey * 3 + 1; i++) {
		for (int j = 1; j < sizex * 3 + 1; j++) {
			char ch = output.at((i * (sizex * 3 + 1)) + j + 1);

			if (ch != '\n') {
				grid[(i * (sizex * 3 + 1)) + j + 1] = ch;
			}
		}
	}

	bool *impasse = (bool*)malloc((sizex) * (sizey) * sizeof(bool));;
	memset(impasse, false, sizey * sizex);

	Cell *c = new Cell{NULL, NULL, startx, starty, heuristic(Point(startx, starty), Point(endx, endy))};
	Point end = Point{endx, endy};

	std::cout << "TEST";
	std::cout << c->x;
	std::cout << (c->x != end.x && c->y != end.y ? "true\n" : "false\n") << '\n';
	while (c->x != end.x && c->y != end.y) {
		auto [c_tmp, impasse_tmp] = search(&grid, &impasse, c, end);
		std::cout << "GOT HERERRERER";
		c = c_tmp;
		impasse = impasse_tmp;
		std::cout << c->x << ", " << c->y << '\n';
		std::cout << "\nGot here6";
	}

	free(grid);

	fstream wf;
	wf.open(argv[2], ios::out);

	wf << output;
	wf.close();

	return 0;
}