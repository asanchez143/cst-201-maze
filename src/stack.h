#include "linkedlist.h"

template <typename T>
class Stack {
	LinkedList<T> *stack;

public:
	Stack() {
		this->stack = new LinkedList<T>();
	}

	void push(T value) {
		this->stack->insertat(0, value);
	}

	T pop() {
		T value = this->stack->get(0)->value;
		this->stack->removeat(0);
		return value;
	}
};