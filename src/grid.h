/// <summary>
///		Point class used for grid creation.
/// </summary>
class Point {
	public:
		int x, y;
		/// <value> x & y coordinates of the node. </value>

		/// <summary>
		///		Point constructor.
		/// </summary>
		///
		/// <param name='x'>
		///		const int value for the x coordinate.
		/// </param>
		///
		/// <param name='y'>
		///		const int value for the y coordinate.
		/// </param>
		Point(const int x, const int y) {
			this->x = x;
			this->y = y;
		}
};
/*
/// <summary>
///		Node class used for pathfinding algorithm.
/// </summary>
class Node {
	public:
		int x, y, cost;
		/// <value> x & y coordinates of the node. </value>

		Node* child = NULL;
		/// <value> The child node. Defaults to NULL. </value>

		/// <summary>
		///		Node constructor. NOTE: Set child later.
		/// </summary>
		///
		/// <param name='x'>
		///		const int value for the x coordinate.
		/// </param>
		///
		/// <param name='y'>
		///		const int value for the y coordinate.
		/// </param>
		Node(const int x, const int y, const int cost) {
			this->x = x;
			this->y = y;
			this->cost = cost;
		}

		/// <summary>
		///		Checks to see if the node has a child.
		/// </summary>
		///
		/// <returns>
		///		Returns a boolean based on whether the child is `NULL`.
		/// </returns>
		bool hasChild() {
			return this->child != NULL;
		}
};*/