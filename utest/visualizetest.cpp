// std dependencies
#include <string>
#include <vector>
#include <iostream>

// Self-dependencies
#include "../src/visualize.cpp"

int main() {
	//std::string teststring = "10, 10.\
(0, 0).\
(9, 9).\
(1, 2), (1, 3), (1, 5),\
(3, 5),\
(6, 8)";

	std::string teststring = "10, 11.\
(0, 0).\
(3, 10).\
(1, 5), (1, 7), (1, 9), (1, 10),\
(2, 2), (2, 3), (2, 4)";

	std::string test = load(teststring);

	std::cout << test;
}