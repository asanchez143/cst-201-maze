// std dependencies
#include <string>
#include <vector>
#include <iostream>

// Self-dependencies
#include "../src/vectors.cpp"

int main() {
	std::string teststring = "(10, 10).\
						   	  (0, 0).\
						   	  (9, 9).\
						   	  (1, 2), (1, 3), (1, 5)";

	std::vector<std::string> splitstring_vector_test = splitstring(teststring, ".", 0, 2);	// Should return ["10,10", "0,0", "9,9"]
	std::string splitstring_single_test = splitstring(teststring, ".", 1);					// Should return "0,0"
	std::vector<Point*> strvectortopoint_test = strvectortopoint(splitstring_vector_test);	// Should return [Point(10, 10), Point(0, 0), Point(9, 9)]

	// Iterating through each split and printing the value
	for (int i = 0; i < splitstring_vector_test.size(); i++) {
		std::cout << splitstring_vector_test.at(i);

		if (i != splitstring_vector_test.size() - 1) {
			std::cout << ", ";
		}
	}

	std::cout << "\n";
	std::cout << splitstring_single_test;
	std::cout << "\n";

	// Iterating through each point and printing it out
	for (int i = 0; i < strvectortopoint_test.size(); i++) {
		Point *p = strvectortopoint_test.at(i);
		std::printf("Point at: %d, %d\n", p->x, p->y);
	}
}